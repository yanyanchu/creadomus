$(document).ready(function() {
	var email = linkTo_UnCryptMailto(getParameterByName('email'));
	var action = getParameterByName('action')!="" ?  getParameterByName('action') : null;
	var colNo = getParameterByName('col')!="" ?  getParameterByName('col') : null;
	
	if(action!=null && colNo!=null){
		if(email.indexOf("@")!=-1){
			initActionSubmit(email,action,colNo);
		}else{
			$('#loading').hide();
			$("#notraining").removeClass("hidden");
		}
	}else{
		if(email.indexOf("@")!=-1){
			initEvent(email);
		}else{
			$('#loading').hide();
			$("#notraining").removeClass("hidden");
		}
		
	}
	//
});


function UnCryptMailto( s )
{
    var n = 0;
    var r = "";
    for( var i = 0; i < s.length; i++)
    {
        n = s.charCodeAt( i );
        if( n >= 8364 )
        {
            n = 128;
        }
        r += String.fromCharCode( n - 1 );
    }
    return r;
}

function linkTo_UnCryptMailto( s )
{
   return UnCryptMailto( s );
}
function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regexS = "[\\?&]" + name + "=([^&#]*)";
	var regex = new RegExp(regexS);
	var results = regex.exec(window.location.href);
	if (results == null)
		return "";
	else
		return decodeURIComponent(results[1].replace(/\+/g, " "));
}


function initEvent(email){
	$.ajax
	   ({
		   cache: false,
	       type: "GET",
	       url: "/creadomus",
	       dataType: 'json',
		   data: {
				   method : 'retrieveUserTraining',
				   email:email
			   },
	       async: true,
	       success: function (response){
				buildTrainingList(response);
	       },error: function(response){
	         // globalErrorMethod(response);
	       }
	    })
	
}

function initActionSubmit(email, action, col){
	$.ajax
	   ({
		   cache: false,
	       type: "GET",
	       url: "/creadomus",
	       dataType: 'json',
		   data: {
				   method : 'submitUserAction',
				   email:email,
				   action:action,
				   col:col
			   },
	       async: true,
	       success: function (response){
				buildTrainingList(response);
	       },error: function(response){
	         // globalErrorMethod(response);
	       }
	    })
	
}


function buildTrainingList(response) {
	$('#loading').hide();
	if (response) {
		$("#training").removeClass("hidden");
		$("#trainingList").empty();
		$("#trainingList").append(response);
		
	}else{
		$("#notraining").removeClass("hidden");
		
	}
}