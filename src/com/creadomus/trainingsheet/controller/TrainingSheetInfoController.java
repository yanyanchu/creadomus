package com.creadomus.trainingsheet.controller;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.creadomus.trainingsheet.util.Util;
import com.google.gson.Gson;

public class TrainingSheetInfoController {

	public static String getTrainingInfo(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		// TODO Auto-generated method stub
		String email = req.getParameter("email");
		System.out.println(email);
		String trainingInfo = getAllTrainingInfo(email);
		
		if(trainingInfo != null){
			Map<String, String> returnJson = new HashMap<String, String>();
			resp.getWriter().println(new Gson().toJson(trainingInfo));
		}
		return null;
		
	}
	
	public static String submitTrainingInfo(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		// TODO Auto-generated method stub
		String email = req.getParameter("email");
		String action = req.getParameter("action");
		String col = req.getParameter("col");
		String trainingInfo = submitToTrainingSpreadsheet(email, action, col);
		
		if(trainingInfo != null){
			Map<String, String> returnJson = new HashMap<String, String>();
			resp.getWriter().println(new Gson().toJson(trainingInfo));
		}
		return null;
		
	}
	public static String getAllTrainingInfo(String email) {
		HttpURLConnection connection = null;
		
		try {
			//URL url = null;
			//			https://script.google.com/macros/s/AKfycbxv-TBUrbh3eQ7eUefqEOzBip699QL_Fg73UvYNlnCiIpepj5U/exec
			//String scriptUrl = "https://script.google.com/macros/s/AKfycby5MinzcJNW8VoCk0JSH5yu_KCqMppGBsXwn4LuIOI3m6Br_ZuR/exec?method=getTrainingInfo&email="+email;
			
			String scriptUrl = "https://script.google.com/macros/s/AKfycby4VrtNeAPWysyeIs3JT39NNUDa08nxb-vlroUgNjyfR4fIJ11h/exec?method=getTrainingInfo&email="+email;
			URL url = new URL(scriptUrl);
			connection = (HttpURLConnection) url.openConnection();
			connection.setConnectTimeout(1 * 60 * 1000); // 1 minute request timeout
			connection.setReadTimeout(5 * 60 * 1000); // 5 minute response timeout
			connection.setRequestMethod("GET");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setAllowUserInteraction(false);
			connection.connect();

			int statuscode = connection.getResponseCode();
			//System.out.println(Util.getStringFromInputStream(connection.getInputStream()));
			String result = Util.getStringFromInputStream(connection.getInputStream());
			if (statuscode == 200) {
				if (result.indexOf("result") >= 0) {
					return result.replace("{", "").replace("}", "").replace("{", "").replace("\"result\"", "").replace(":\"", "").replace("\"", "");
				} else {
					return null;
					//log.log(Level.WARNING, "Google App Script returned the response " + responseCode + " while processing the Spreadsheet report: " + result);
				}				
			} else {
				//log.log(Level.WARNING, "Google App Script returned the response " + responseCode + " while processing the Spreadsheet report: " + result);
			}
		} catch (Exception ex) {
			// handle exception here
			return null;
		} finally {
			connection = null;
		}
		return null;

	}
	public static String submitToTrainingSpreadsheet(String email, String action, String col) {
		// TODO Auto-generated method stub
		HttpURLConnection connection = null;
		
		try {
			//URL url = null;
			//			https://script.google.com/macros/s/AKfycbxv-TBUrbh3eQ7eUefqEOzBip699QL_Fg73UvYNlnCiIpepj5U/exec
			String scriptUrl = "https://script.google.com/macros/s/AKfycby4VrtNeAPWysyeIs3JT39NNUDa08nxb-vlroUgNjyfR4fIJ11h/exec?method=submitTrainingInfo&email="+email+
					"&action="+action+"&col="+col;
			URL url = new URL(scriptUrl);
			connection = (HttpURLConnection) url.openConnection();
			connection.setConnectTimeout(1 * 60 * 1000); // 1 minute request timeout
			connection.setReadTimeout(5 * 60 * 1000); // 5 minute response timeout
			connection.setRequestMethod("GET");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setAllowUserInteraction(false);
			connection.connect();

			int statuscode = connection.getResponseCode();
			//System.out.println(Util.getStringFromInputStream(connection.getInputStream()));
			String result = Util.getStringFromInputStream(connection.getInputStream());
			if (statuscode == 200) {
				if (result.indexOf("result") >= 0) {
					return result.replace("{", "").replace("}", "").replace("{", "").replace("\"result\"", "").replace(":\"", "").replace("\"", "");
				} else {
					return null;
					//log.log(Level.WARNING, "Google App Script returned the response " + responseCode + " while processing the Spreadsheet report: " + result);
				}				
			} else {
				//log.log(Level.WARNING, "Google App Script returned the response " + responseCode + " while processing the Spreadsheet report: " + result);
			}
		} catch (Exception ex) {
			// handle exception here
			return null;
		} finally {
			connection = null;
		}
		return null;
		
	}

	public static void testEmailTemplate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		// TODO Auto-generated method stub
		HttpURLConnection connection = null;
		
		try {
			//URL url = null;
			//			https://script.google.com/macros/s/AKfycbxv-TBUrbh3eQ7eUefqEOzBip699QL_Fg73UvYNlnCiIpepj5U/exec
			String scriptUrl = "https://script.google.com/macros/s/AKfycby4VrtNeAPWysyeIs3JT39NNUDa08nxb-vlroUgNjyfR4fIJ11h/exec?method=testEmail";
			URL url = new URL(scriptUrl);
			connection = (HttpURLConnection) url.openConnection();
			connection.setConnectTimeout(1 * 60 * 1000); // 1 minute request timeout
			connection.setReadTimeout(5 * 60 * 1000); // 5 minute response timeout
			connection.setRequestMethod("GET");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setAllowUserInteraction(false);
			connection.connect();

			int statuscode = connection.getResponseCode();
			//System.out.println(Util.getStringFromInputStream(connection.getInputStream()));
			String result = Util.getStringFromInputStream(connection.getInputStream());
			if (statuscode == 200) {
				//if (result.indexOf("result") >= 0) {
					resp.getWriter().println("Email sent");
				/*} else {
					resp.getWriter().println("Email not sent");
					//log.log(Level.WARNING, "Google App Script returned the response " + responseCode + " while processing the Spreadsheet report: " + result);
				}	*/			
			} else {
				//log.log(Level.WARNING, "Google App Script returned the response " + responseCode + " while processing the Spreadsheet report: " + result);
			}
		} catch (Exception ex) {
			// handle exception here
			//resp.getWriter().println("Error in sending email");
		} finally {
			connection = null;
		}
		//resp.getWriter().println("Error in sending email");
		
	}
	
	/*private boolean exportDataToSpreadSheet(String GsonData, String paramlogFileId){
		try{
			
			
			if(paramlogFileId != null && paramlogFileId != ""){
				
				String params = "id=" + paramlogFileId;
				String scriptUrl = "";
				if(Util.isProduction()){
					scriptUrl = "https://script.google.com/macros/s/AKfycbwU20DYWW1DFrQ4bDo87naP0ys2mgf5RnsmBLmlZ6uNW2VVKRQ/exec?";
				}else{
					scriptUrl = "https://script.google.com/macros/s/AKfycbzkukeTZQOpw8XY2t443n9MdYCl-sQbmL3-twbUZm3-z8TVF-ur/exec?";
				}
				scriptUrl = "https://script.google.com/macros/s/AKfycby5MinzcJNW8VoCk0JSH5yu_KCqMppGBsXwn4LuIOI3m6Br_ZuR/exec?";
				
				URL url = new URL(scriptUrl + params);
				
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				connection.setConnectTimeout(5 * 60 * 1000); // 5 minute request timeout
				connection.setReadTimeout(5 * 60 * 1000); // 5 minute request timeout
				connection.setRequestMethod("POST");
				connection.setUseCaches(false);
				connection.setDoInput(true);
				connection.setDoOutput(true);
				connection.setAllowUserInteraction(false);
				connection.setRequestProperty("Content-Type", "application/json; charset=utf8");
				connection.setRequestProperty("Content-Length", String.valueOf(GsonData.length()));
				
				OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
				wr.write(GsonData);
				wr.flush();
				wr.close();
				
				int responseCode = connection.getResponseCode();
				String result = Util.getStringFromInputStream(connection.getInputStream());
				if (responseCode == 200) {			
					if (result.indexOf("success") >= 0) {
						
					} else {
						//log.log(Level.WARNING, "Google App Script returned the response " + responseCode + " while processing the Spreadsheet report: " + result);
					}				
				} else {
					//log.log(Level.WARNING, "Google App Script returned the response " + responseCode + " while processing the Spreadsheet report: " + result);
				}
				
			}
		}catch(Exception ex){
			log.log(Level.SEVERE, ex.getMessage(), ex);
			ex.printStackTrace();
			return false;
		}
		return true;
		
	}*/
}
