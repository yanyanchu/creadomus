package com.creadomus.trainingsheet;

//package com.mincko.freshdesk;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.apache.commons.codec.binary.Base64;

import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class FreshDeskUpdate {
	public static void main(String[] args) throws JSONException, MalformedURLException, IOException {

		//String webPage = "https://fouriconsultancy.freshdesk.com/helpdesk/tickets/summary.xml?view_name=open";
		//String webPage = "https://fouriconsultancy.freshdesk.com/api/v1/custom_fields";
		String name = "7hqlDQ3EwZA322yIcCO";
		String password = "X";
		//String payload = "{\"custom_fields\":{\"application\":null,\"module\":null,\"company\":\"TG\",\"credit\":null}}";
		String payload = "{\"custom_fields\":{\"cf_rsultat_de_lopportunit\":\"Rendez-vous pris avec conseiller (domicile ou bureau)\"}}";
		String webPage = "https://hhplu.freshdesk.com/api/v2/tickets/";
		
		String authString = name + ":" + password;
		//System.out.println("auth string: " + authString);
		byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
		String authStringEnc = new String(authEncBytes);
	//	System.out.println("Base64 encoded auth string: " + authStringEnc);

		StringBuffer lonContent = new StringBuffer();
		//String data=getAllTickets(10);
			for(int page=120; page<150; page++){
				String data=getAllTickets(page);
				JsonParser jsonParser = new JsonParser();
				JsonArray jsonArray = (JsonArray) jsonParser.parse(data);
				//System.out.println("&"+jsonArray);
				 JSONArray jsonArr = new JSONArray(data);
				
				// for (int i = 19; i < jsonArr.length(); i++)
				 for (int i = 0; i < jsonArr.length(); i++)
			        {
					 JSONObject jsonObj = jsonArr.getJSONObject(i);
					 String id = jsonObj.getString("id");
					 String jsonCustom = jsonObj.getJSONObject("custom_fields").toString();
					 JsonObject o = new JsonParser().parse(jsonCustom).getAsJsonObject();
					 //String payload = "{\"custom_fields\":{\"cf_rsultat_de_lopportunit\":\"Rendez-vous pris avec conseiller (domicile ou bureau)\"}}";
					 for(Map.Entry<String, JsonElement> entry : o.entrySet()) {
						 if(entry.getKey().equalsIgnoreCase("cf_rsultat_de_lopportunit")){
							// System.out.println(entry.getValue());
							 if(!entry.getValue().isJsonNull()){
							
								// if(entry.getValue().getAsString().replaceAll(" ","").replaceAll("\n","").equalsIgnoreCase("Rendez-vousprisavecconseiller")){
								 if(entry.getValue().getAsString().equalsIgnoreCase("Rendez-vous pris avec conseiller")){
									 StringBuffer p1Content = new StringBuffer();
									 //System.out.println(entry.getKey());
								       // System.out.println(entry.getValue());
								       // System.out.println(id +" - "+i);
									 //"requester_id":1004865939
								       /* Random random = new Random();
								        URL url2 = new URL("https://hhplu.freshdesk.com/api/v2/tickets/"+id);
								        HttpURLConnection connection = (HttpURLConnection) url2.openConnection();
								        connection.setRequestMethod("PUT");
								        connection.setDoOutput(true);
								        connection.setRequestProperty("Authorization", "Basic " + authStringEnc);
								        connection.setRequestProperty("Content-Type", "application/json");
								        connection.setRequestProperty("Accept", "application/json");
								        OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
								        osw.write(String.format(payload, random.nextInt(30), random.nextInt(20)));
								        //osw.write(String.format("{\"custom_fields\":{\"company\":TG}", random.nextInt(30), random.nextInt(20)));
								        osw.flush();
								        osw.close();
								    	p1Content.append(id + " status:" +connection.getResponseCode()); //1.01*/
								        p1Content.append(id);
								    	p1Content.append("\r\n");
								    	lonContent.append(p1Content);
								        //System.err.println(connection.getResponseCode());
								 }
									
							 }
							 
							 
						 }
					       
					    }
					
					
					
			        }
			}
		
			
			System.out.println("*** FreshdeskID ***"+lonContent);
	}
	
	public static String getAllTickets(int page) throws JSONException{

		try {
			//String webPage = "https://fouriconsultancy.freshdesk.com/helpdesk/tickets/summary.xml?view_name=open";
			//String webPage = "https://fouriconsultancy.freshdesk.com/api/v1/custom_fields";
			String name = "7hqlDQ3EwZA322yIcCO";
			String password = "X";
			//String payload = "{\"custom_fields\":{\"application\":null,\"module\":null,\"company\":\"TG\",\"credit\":null}}";
			//String payload = "{\"custom_fields\":{\"company\":\"TG\"}}";
			String webPage = "https://hhplu.freshdesk.com/api/v2/tickets?per_page=30&page="+page+"&updated_since=2014-01-19T02:00:00Z";
			//String webPage = "https://fouriconsultancy.freshdesk.com/helpdesk/tickets/summary.xml?view_name=open";
			String authString = name + ":" + password;
			//System.out.println("auth string: " + authString);
			byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
			String authStringEnc = new String(authEncBytes);
			//System.out.println("Base64 encoded auth string: " + authStringEnc);

			URL url = new URL(webPage);
			//URLConnection urlConnection = (URLConnection) url.openConnection();
			
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setDoOutput(true);
			urlConnection.setRequestMethod("GET");
			//urlConnection.setRequestMethod("put");
			//urlConnection.setRequestMethod("PUT");
			
			urlConnection.setRequestProperty("Authorization", "Basic " + authStringEnc);
			InputStream is = urlConnection.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			
			 /*Random random = new Random();
		        URL url2 = new URL("https://fouriconsultancy.freshdesk.com/api/v2/tickets/8965");
		        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		       // connection.setRequestMethod("PUT");
		        connection.setDoOutput(true);
		        connection.setRequestProperty("Authorization", "Basic " + authStringEnc);
		        connection.setRequestProperty("Content-Type", "application/json");
		        connection.setRequestProperty("Accept", "application/json");*/
		       /* OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
		        osw.write(String.format(payload, random.nextInt(30), random.nextInt(20)));
		        //osw.write(String.format("{\"custom_fields\":{\"company\":TG}", random.nextInt(30), random.nextInt(20)));
		        osw.flush();
		        osw.close();
		        System.err.println(connection.getResponseCode());*/
		        
		        int numCharsRead;
				char[] charArray = new char[1024];
				StringBuffer sb = new StringBuffer();
				/*while ((numCharsRead = isr.read(charArray)) > 0) {
					sb.append(charArray, 0, numCharsRead);
				}
				String result = sb.toString();*/
				BufferedReader br = null;
				//StringBuilder sb = new StringBuilder();
				String line;
				try {

				    br = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
				    while ((line = br.readLine()) != null) {
					sb.append(line);
				    }

				} catch (IOException e) {
				    e.printStackTrace();
				} finally {
				    if (br != null) {
					try {
					    br.close();
					} catch (IOException e) {
					    e.printStackTrace();
					}
				    }
				}

				String result = sb.toString();
			//	System.out.println("*** BEGIN ***");
			//	System.out.println(result);
				Gson gson = new Gson();
				String data=result;
				return data;
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	public static String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

		    br = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
		    while ((line = br.readLine()) != null) {
			sb.append(line);
		    }

		} catch (IOException e) {
		    e.printStackTrace();
		} finally {
		    if (br != null) {
			try {
			    br.close();
			} catch (IOException e) {
			    e.printStackTrace();
			}
		    }
		}

		return sb.toString();

	    }
}
