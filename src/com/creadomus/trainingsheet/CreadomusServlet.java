package com.creadomus.trainingsheet;

import java.io.IOException;
import javax.servlet.http.*;

import com.creadomus.trainingsheet.controller.TrainingSheetInfoController;

@SuppressWarnings("serial")
public class CreadomusServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String method = req.getParameter("method") != null ? req.getParameter("method") : "";


		if (method.equalsIgnoreCase("retrieveUserTraining")) {
			TrainingSheetInfoController.getTrainingInfo(req, resp);
			return;
		}else if (method.equalsIgnoreCase("submitUserAction")) {
			TrainingSheetInfoController.submitTrainingInfo(req, resp);
			return;
		}else if (method.equalsIgnoreCase("testEmailTemplate")) {
			TrainingSheetInfoController.testEmailTemplate(req, resp);
			return;
		}else{
			
			return;
		}
	}
}
